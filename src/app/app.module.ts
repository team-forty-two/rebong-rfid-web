import { NgModule } from '@angular/core';
import { RouterModule, Routes, CanActivate } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule  } from 'angularfire2/database';
import { environment } from '../environments/environment';
import { Angular2FontawesomeModule } from 'angular2-fontawesome/angular2-fontawesome';
import { AngularFireAuth, AngularFireAuthModule } from 'angularfire2/auth';

import { AppComponent } from './app.component';
import { FlightComponent } from './flights/flight/flight.component';
import { FlightsService } from './flights/flights.service';
import { FlightsComponent } from './flights/flights.component';
import { PassengerComponent } from './passengers/passenger/passenger.component';
import { BaggageComponent } from './passengers/passenger/baggage/baggage.component';
import { AddEditPassengerComponent } from './passengers/add-edit-passenger/add-edit-passenger.component';
import { LoginComponent } from './auth/login/login.component';
import { AuthGuard } from './auth/auth-guard.service';
import { AuthService } from './auth/auth.service';
import { PassengerService } from './passengers/passenger.service';
import { PassengersComponent } from './passengers/passengers.component';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './users/user/user.component';
import { AddEditFlightComponent } from './flights/add-edit-flight/add-edit-flight.component';
import { AddEditUserComponent } from './users/add-edit-user/add-edit-user.component';
import { DevicesComponent } from './devices/devices.component';
import { DeviceComponent } from './devices/device/device.component';
import { AddEditDeviceComponent } from './devices/add-edit-device/add-edit-device.component';
import { DeviceService } from './devices/device.service';
import { UserService } from './users/user.service';
import { FieldErrorDisplayComponent } from './shared/field-error-display/field-error-display.component';

const appRoutes: Routes = [
  {path: '', redirectTo: '/flights', pathMatch: 'full' },
  {path: 'flights', component: FlightsComponent, canActivate:[AuthGuard] },
  {path: 'flights/:id', component: PassengersComponent, canActivate:[AuthGuard] },
  {path: 'devices', component: DevicesComponent, canActivate:[AuthGuard] },
  {path: 'users', component: UsersComponent, canActivate:[AuthGuard] },
  {path: 'login', component: LoginComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    FlightComponent,
    FlightsComponent,
    PassengersComponent,
    PassengerComponent,
    BaggageComponent,
    AddEditPassengerComponent,
    LoginComponent,
    UsersComponent,
    UserComponent,
    AddEditFlightComponent,
    AddEditUserComponent,
    DevicesComponent,
    DeviceComponent,
    AddEditDeviceComponent,
    FieldErrorDisplayComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    RouterModule.forRoot(appRoutes),
    Angular2FontawesomeModule,
    FormsModule,
  ],
  entryComponents: [
    AddEditPassengerComponent, 
    AddEditFlightComponent, 
    AddEditUserComponent, 
    AddEditDeviceComponent
  ],
  providers: [
    FlightsService, 
    PassengerService, 
    AuthService, 
    AuthGuard, 
    UserService, 
    DeviceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
