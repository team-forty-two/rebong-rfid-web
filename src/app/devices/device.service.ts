import { Device } from './device.model';
import { AppUrl } from '../shared/firebase-uri.model';
import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Flight } from '../flights/flight/flight.model';

@Injectable()
export class DeviceService {
  constructor(private firebase: AngularFireDatabase) { }
  
  fetchDevices() {
    return this.firebase.object(AppUrl.devices).snapshotChanges()
      .map(res => {
          const devices = res.payload.val();
          let convertedDevices: Device[] = [];
          if (devices) {
              Object.keys(devices)
                  .forEach(id => {
                    devices[id].id = id;
                    convertedDevices.push(devices[id]);
                  });
          }
  
          return convertedDevices;
      });
  }

  addDevice(device: Device) {
    this.firebase.list(AppUrl.devices)
        .set(device.id, {
          'conveyor': device.conveyor
        });
  }

  updateDevice(deviceId: string, device: Device) {
    this.firebase.object(AppUrl.devices + '/' + deviceId)
      .update({
        'conveyor': device.conveyor
      });
  }

  deleteDevice(device: Device) {
    this.firebase.list(AppUrl.devices)
      .set(device.id, null);
    
    if(device.flights) {
      Object.keys(device.flights)
        .forEach(flight => {
          this.firebase.object(AppUrl.flights + '/' + flight 
            + '/' + AppUrl.devices + '/' + device.id).set(null);
        });
    }
  }

  bindBaggageAndDevice(deviceId: string, baggageId: string) {
    this.firebase.object(AppUrl.devices + '/' + deviceId + '/' + AppUrl.baggages)
      .update({
          [baggageId] : true
      });
    
    this.firebase.object(AppUrl.baggages + '/' + baggageId + '/' + AppUrl.devices)
      .update({
        [deviceId]: true
      })
  }
}
