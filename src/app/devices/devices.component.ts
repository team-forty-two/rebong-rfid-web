import { Device } from './device.model';
import { DeviceService } from './device.service';
import { AddEditDeviceComponent } from './add-edit-device/add-edit-device.component';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-devices',
  templateUrl: './devices.component.html',
  styleUrls: ['./devices.component.css']
})
export class DevicesComponent implements OnInit {

  devices: Device[];
  filteredDevices: Device[];
  
  constructor(private fb: FormBuilder,
    private modalService: NgbModal,
    private deviceService: DeviceService) { }

  ngOnInit() {
    this.deviceService.fetchDevices()
      .subscribe((devices: Device[]) => {
        console.log(devices);
        this.devices = devices;
        this.filteredDevices = this.devices.slice(0);
      });
  }

  openAddModal() {
    const modal = this.modalService.open(AddEditDeviceComponent);
  }

}
