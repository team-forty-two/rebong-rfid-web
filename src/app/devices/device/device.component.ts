import { AddEditDeviceComponent } from './../add-edit-device/add-edit-device.component';
import { DeviceService } from './../device.service';
import { Device } from './../device.model';
import { Component, OnInit, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-device',
  templateUrl: './device.component.html',
  styleUrls: ['./device.component.css']
})
export class DeviceComponent implements OnInit {

  @Input() device: Device;
  
  constructor(private deviceService: DeviceService,
    private modalService: NgbModal) { }

  ngOnInit() {
  }

  deleteUser(id: string) {
    this.deviceService.deleteDevice(this.device);
  }

  openViewUpdateModal(device: Device) {
    const modal = this.modalService.open(AddEditDeviceComponent);
    modal.componentInstance.device = device;
  }

}
