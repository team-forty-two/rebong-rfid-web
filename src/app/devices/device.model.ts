import { Flight } from "../flights/flight/flight.model";

export class Device {
    id: string;
    conveyor: string;
    flights: any;

    constructor(id: string, conveyor: string, flights: any) {
        this.id = id;
        this.conveyor = conveyor;
        this.flights = flights;
    }
}