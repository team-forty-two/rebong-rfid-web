import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { Device } from '../device.model';
import { DeviceService } from './../device.service';

@Component({
  selector: 'app-add-edit-device',
  templateUrl: './add-edit-device.component.html',
  styleUrls: ['./add-edit-device.component.css']
})
export class AddEditDeviceComponent implements OnInit {

  modalTitle: string = "Add Device";
  device: Device;
  isAddRequest: boolean = true;
  newDeviceForm: FormGroup = this.fb.group({
    id: ["", Validators.required],
    conveyor: ["", Validators.required]
  });

  constructor(private fb: FormBuilder,
    private deviceService: DeviceService,
    public activeModal: NgbActiveModal) { }

  ngOnInit() {
    if(this.device) {
      this.isAddRequest = false;
      this.modalTitle = 'Update Device';
      this.newDeviceForm.controls['id'].setValue(this.device.id);
      this.newDeviceForm.controls['conveyor'].setValue(this.device.conveyor);
      this.newDeviceForm.controls['id'].disable();
    }
  }

  onSubmit() {
    if (this.newDeviceForm.valid) {
      if(this.device) {
        const device: Device = this.newDeviceForm.value;
        this.deviceService.updateDevice(this.device.id, device);
      } else {
        const device: Device = this.newDeviceForm.value;
        this.deviceService.addDevice(device);
      }
  
      this.newDeviceForm.reset();
      this.activeModal.close();
    } else {
      this.validateAllFormFields(this.newDeviceForm);      
    }
    
  }

  isFieldValid(field: string) {
    console.log(this.newDeviceForm.get(field));
    return !this.newDeviceForm.get(field).valid && this.newDeviceForm.get(field).touched;
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  getErrorMessage(field: string) {
    if(this.isFieldValid(field)) {
      const errors = this.newDeviceForm.get(field).errors;
      let errorMsgs = [];

      Object.keys(errors).forEach(error => {
        if(errors[error]) {
          if(error == 'required') {
            errorMsgs.push('Please enter your ' + field);
          } else {
            if (error == 'email') {
              errorMsgs.push('Please enter correct email address');
            } else if (error == 'pattern') {
              errorMsgs.push('Please enter valid ' + field);
            }
          }
        }
      })

      return errorMsgs;
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

}
