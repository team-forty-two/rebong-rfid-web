import { Injectable } from '@angular/core';

import { AuthService } from './../auth/auth.service';
import { User } from './user.model';
import { AngularFireDatabase } from 'angularfire2/database';

@Injectable()
export class UserService {
  userUrl: string = "users";

  constructor(private authService: AuthService,
    private firebase: AngularFireDatabase) { }

  
  fetchUsers() {
    return this.firebase.object(this.userUrl).snapshotChanges()
      .map(res => {
          const users = res.payload.val();
          let convertedUser: User[] = [];
          if (users) {
              Object.keys(users)
                  .forEach(id => {
                    users[id].id = id;
                    convertedUser.push(users[id]);
                  });
          }
  
          return convertedUser;
      });
  }
  
  addUser(user: User) {
    this.authService.signUp(user.email, user.password)
      .then(response => {
        const id = response.uid;
        this.firebase.list(this.userUrl)
          .set(id, {
            name: user.name,
            email: user.email
          });
      }).catch(error => {
        console.log(error);
      })
  }

  updateUser(id: string, newUser: User) {
    this.firebase.object(this.userUrl + '/' + id)
      .update(newUser).catch(response => {
        
      })
  }

  deleteUser(id: string) {
    this.firebase.list(this.userUrl).set(id, null)
      .then(response => {
        
      }).catch(error => {
        console.log(error);
      })
      
  }
}
