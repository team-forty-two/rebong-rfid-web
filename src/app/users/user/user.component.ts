import { UserService } from './../user.service';
import { Component, OnInit, Input } from '@angular/core';

import { User } from '../user.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AddEditUserComponent } from '../add-edit-user/add-edit-user.component';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  @Input() user: User;
  
  constructor(private userService: UserService,
    private modalService: NgbModal) { }

  ngOnInit() {
  }

  deleteUser(id: string) {
    this.userService.deleteUser(id);
  }

  openViewUpdateModal(user: User) {
    const modal = this.modalService.open(AddEditUserComponent);
    modal.componentInstance.user = user;
  }

}
