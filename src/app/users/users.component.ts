import { AddEditUserComponent } from './add-edit-user/add-edit-user.component';
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { UserService } from './user.service';
import { AuthService } from './../auth/auth.service';
import { User } from './user.model';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users: User[];
  filteredUser: User[];
  
  constructor(private fb: FormBuilder,
    private modalService: NgbModal,
    private userService: UserService) { }

  ngOnInit() {
    this.userService.fetchUsers()
      .subscribe((users: User[]) => {
        this.users = users;
        this.filteredUser = this.users.slice(0);
      });
  }

  openAddModal() {
    const modal = this.modalService.open(AddEditUserComponent);
  }

}
