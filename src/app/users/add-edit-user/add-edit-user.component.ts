import { UserService } from './../user.service';
import { FormBuilder, FormGroup, Validators, NgForm, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { User } from '../user.model';

@Component({
  selector: 'app-add-edit-user',
  templateUrl: './add-edit-user.component.html',
  styleUrls: ['./add-edit-user.component.css']
})
export class AddEditUserComponent implements OnInit {
  modalTitle: string = "Add User";
  user: User;
  isAddRequest: boolean = true;
  newUserForm: FormGroup = this.fb.group({
    name: ["", [Validators.required]],
    email: ["", [Validators.required, Validators.email]],
    password: ["", [Validators.required]]
  });

  constructor(private fb: FormBuilder,
    private userService: UserService,
    public activeModal: NgbActiveModal) { }

  ngOnInit() {
    if(this.user) {
      this.isAddRequest = false;
      this.modalTitle = 'Update User';
      this.newUserForm.controls['name'].setValue(this.user.name);
      this.newUserForm.controls['email'].setValue(this.user.email);
      this.newUserForm.controls['password'].setValue(this.user.password);
      this.newUserForm.controls['email'].disable();
    }
  }

  onSubmit() {
    if (this.newUserForm.valid) {
      if(this.user) {
        const user: User = this.newUserForm.value;
      } else {
        const user: User = this.newUserForm.value;
        this.userService.addUser(user);
      }
      this.activeModal.dismiss('Cross click');
      this.newUserForm.reset();
    } else {
      console.log('validate');
      this.validateAllFormFields(this.newUserForm);      
    }
  }

  isFieldValid(field: string) {
    console.log(this.newUserForm.get(field));
    return !this.newUserForm.get(field).valid && this.newUserForm.get(field).touched;
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  getErrorMessage(field: string) {
    if(this.isFieldValid(field)) {
      const errors = this.newUserForm.get(field).errors;
      let errorMsgs = [];

      Object.keys(errors).forEach(error => {
        if(errors[error]) {
          if(error == 'required') {
            errorMsgs.push('Please enter your ' + field);
          } else {
            if (error == 'email') {
              errorMsgs.push('Please enter correct email address');
            } else if (error == 'pattern') {
              errorMsgs.push('Please enter valid ' + field);
            }
          }
        }
      })

      return errorMsgs;
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

}
