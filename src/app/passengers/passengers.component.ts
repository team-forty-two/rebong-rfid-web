import { AddEditFlightComponent } from './../flights/add-edit-flight/add-edit-flight.component';
import { Passenger } from './passenger/passenger.model';
import { PassengerService } from './passenger.service';
import { Flight } from './../flights/flight/flight.model';
import { FlightsService } from './../flights/flights.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { FormGroup, Validators, FormBuilder, FormArray, FormGroupDirective } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AddEditPassengerComponent } from './add-edit-passenger/add-edit-passenger.component';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Component({
  selector: 'app-passengers',
  templateUrl: './passengers.component.html',
  styleUrls: ['./passengers.component.css']
})
export class PassengersComponent implements OnInit {
  flightNumber: string;
  flight: BehaviorSubject<Flight> = new BehaviorSubject<Flight>(null);
  flightDetails: Flight;
  passengers: Passenger[];
  filteredPassengers: Passenger[];
  searchText: string = "";
  fightDetails: Flight;

  constructor(private activatedRoute: ActivatedRoute,
    private flightsService: FlightsService,
    private passengerService: PassengerService,
    private modalService: NgbModal,
    private fb: FormBuilder) { }

  ngOnInit() {
    this.flightNumber = this.activatedRoute.snapshot.params['id'];
    this.flightsService.fetchFlight(this.flightNumber)
      .subscribe(flight => {
        this.flight.next(flight);
      })

    this.passengerService.fetchPassengers()
      .subscribe((passengers: Passenger[]) => {
        this.passengers = passengers.filter((passenger: Passenger) => {
          if(passenger.flight[this.flightNumber]) return passenger;
        });
        this.filteredPassengers = this.passengers.slice(0);
      });
  }

  filterPassenger(text) {
    this.filteredPassengers = this.passengers
      .filter(passenger => {
        if(passenger.firstName.toLowerCase().indexOf(text) > -1
          || passenger.lastName.toLowerCase().indexOf(text) > -1) {
            return passenger;
          }
      }).slice(0);
  }

  openAddModal() {
    const modal = this.modalService.open(AddEditPassengerComponent);
    modal.componentInstance.flight = this.flight.value;
  }

  openFlightUpdateModal(flightDetails: Flight) {
    const modal = this.modalService.open(AddEditFlightComponent);
    modal.componentInstance.flightDetails = flightDetails;
    modal.componentInstance.isAddRequest = false;
  }
}
