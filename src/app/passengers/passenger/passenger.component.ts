import { Flight } from './../../flights/flight/flight.model';
import { PassengerService } from './../passenger.service';
import { Passenger } from './passenger.model';
import { Component, OnInit, Input } from '@angular/core';
import { AddEditPassengerComponent } from '../add-edit-passenger/add-edit-passenger.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-passenger',
  templateUrl: './passenger.component.html',
  styleUrls: ['./passenger.component.css']
})
export class PassengerComponent implements OnInit {
  @Input() passenger: Passenger;
  @Input() flight: Flight;

  constructor(private passengerService: PassengerService,
    private modalService: NgbModal) { }

  ngOnInit() {
  }

  deleteUser(id: string) {
     this.passengerService.deletePassenger(this.flight, id); 
  }

  openViewUpdateModal(passengerId: string, passengerData: Passenger) {
    const modal = this.modalService.open(AddEditPassengerComponent);
    modal.componentInstance.flight = this.flight;
    modal.componentInstance.passengerId = passengerId;
    modal.componentInstance.passenger = passengerData;
  }

}
