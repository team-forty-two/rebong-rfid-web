export class Passenger {
    id: string;
    firstName: string;
    lastName: string;
    contactNumber: string;
    flight: any;
    baggages: any;
    
    constructor(firstName: string, lastName: string, 
        contactNumber: string, flight: any, baggages: any) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.contactNumber = contactNumber;
        this.flight = flight;
        this.baggages = baggages;
    }

}