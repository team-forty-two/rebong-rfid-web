import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PassengerService } from './../passenger.service';
import { Passenger } from './../passenger/passenger.model';
import { Flight } from '../../flights/flight/flight.model';

@Component({
  selector: 'app-add-edit-passenger',
  templateUrl: './add-edit-passenger.component.html',
  styleUrls: ['./add-edit-passenger.component.css']
})
export class AddEditPassengerComponent implements OnInit {
  modalTitle: string = 'Add Passenger';
  objectKeys = Object.keys;
  passenger: Passenger;
  passengerId: string;
  flightNumber: string;
  newUserForm: FormGroup = this.fb.group({
    firstName: ["", Validators.required],
    lastName: ["", Validators.required],
    contactNumber: ["", [Validators.required, Validators.pattern('^[0-9]+$')]],
    baggages: this.fb.group({

    })
  });
  isAddRequest: boolean = true;
  flight: Flight;
  emptyBaggageName: boolean = false;

  @ViewChild('baggage') baggageInput: ElementRef; 

  constructor(private fb: FormBuilder,
    private passengerService: PassengerService,
    public activeModal: NgbActiveModal) { }

  ngOnInit() {
    if(this.passenger) {
      this.isAddRequest = false;
      this.modalTitle = 'Update Passenger';
      this.newUserForm.controls['firstName'].setValue(this.passenger.firstName);
      this.newUserForm.controls['lastName'].setValue(this.passenger.lastName);
      this.newUserForm.controls['contactNumber'].setValue(this.passenger.contactNumber);

      if(this.passenger.baggages) {
        Object.keys(this.passenger.baggages)
          .forEach(baggage => {
            const baggageData = this.passengerService.fetchBaggage(baggage)
              .subscribe(data => {
                if(data) {
                  const status = data.status ? data.status : 0;
                  const description = data.description ? data.description : '';
                  const control = <FormGroup>this.newUserForm.controls['baggages'];
                  control.addControl(baggage, this.fb.group({
                    status: status,
                    description: description
                  }));
                }
              })
          });
      }
    }
  }

  onSubmit() {
    if (this.newUserForm.valid) {
      if(this.isAddRequest) {
        this.passengerService.addPassenger(this.flight, this.newUserForm.value);
      } else {
        this.passengerService.updatePassenger(this.flight, this.passengerId, this.newUserForm.value);
      }

      this.newUserForm.reset();
      this.activeModal.close();
    } else {
      this.validateAllFormFields(this.newUserForm);      
    }
  }

  onUpdate() {

  }

  addBaggage(rfidField: HTMLInputElement, baggageDesc: HTMLInputElement) {
    const rfid = rfidField.value
    const baggageDescription = baggageDesc.value;
    if(rfid && rfid != '') {
      const control = <FormGroup>this.newUserForm.controls['baggages'];
      control.addControl(rfid, this.fb.group({
        status: 1,
        description: baggageDescription
      }));
      rfidField.value = "";
      baggageDesc.value = "";
    } else {
      this.emptyBaggageName = true;
    }
    
  }

  removeBaggage(i: string) {
    const control = <FormGroup>this.newUserForm.controls['baggages'];
    control.removeControl(i);
  }

  removeLeadingZero() {
    let value: string = this.baggageInput.nativeElement.value;
    if(value.startsWith('0')) {
      value = value.slice(1, value.length);
      this.baggageInput.nativeElement.value = value;
    }
  }

  isFieldValid(field: string) {
    return !this.newUserForm.get(field).valid && this.newUserForm.get(field).touched;
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  getErrorMessage(field: string) {
    if(this.isFieldValid(field)) {
      const errors = this.newUserForm.get(field).errors;
      let errorMsgs = [];

      Object.keys(errors).forEach(error => {
        const fieldName = field.replace(/([a-z])([A-Z])/g, '$1 $2').toLowerCase();
        if(errors[error]) {
          if(error == 'required') {
            errorMsgs.push('Please enter your ' + fieldName);
          } else {
            if (error == 'email') {
              errorMsgs.push('Please enter correct email address');
            } else if (error == 'pattern') {
              errorMsgs.push('Please enter valid ' + fieldName);
            }
          }
        }
      })

      return errorMsgs;
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

}
