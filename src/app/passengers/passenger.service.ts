import { Flight } from './../flights/flight/flight.model';
import { AppUrl } from './../shared/firebase-uri.model';
import { FlightsService } from './../flights/flights.service';
import { AngularFireDatabase } from 'angularfire2/database';
import { Injectable } from '@angular/core';
import { Passenger } from './passenger/passenger.model';
import { DeviceService } from '../devices/device.service';

@Injectable()
export class PassengerService {

    constructor(private firebase: AngularFireDatabase,
        private flightsService: FlightsService,
        private deviceService: DeviceService) { }

    fetchPassengers() {
        return this.firebase.object(AppUrl.passengers).snapshotChanges()
            .map(res => {
                const passengers = res.payload.val();
                let convertedPassengers: Passenger[] = [];
                if (passengers) {
                    Object.keys(passengers)
                        .forEach(id => {
                            passengers[id].id = id;
                            convertedPassengers.push(passengers[id]);
                        });
                }

                return convertedPassengers;
            });
    }

    addPassenger(flight: Flight, user: Passenger) {
        const baggages: any = [];
        const userbaggages: any = {};
        user.flight = {
            [flight.number]: true
        };
        // Get baggageIds to be used when adding
        // Also create baggage/user object 
        Object.keys(user.baggages)
            .forEach((baggage) => {
                baggages.push({
                    id: baggage,
                    description: user.baggages[baggage].description
                });
                userbaggages[baggage] = true;
            });
        //replace baggage list with object
        user.baggages = userbaggages;

        const id = this.firebase.list(AppUrl.passengers).push(user)
            .key;
        this.flightsService.addUser(flight.number, id);
        baggages.forEach((baggage) => {
            this.addBaggage(baggage, id);
        });

        //bind baggages and device
        this.bindBaggageAndDevice(flight, baggages);
    }

    updatePassenger(flight: Flight, userId: string, newPassenger: Passenger) {
        //Fetch previous user data
        const passengerObservable = this.fetchPassenger(userId)
            .subscribe(data => {
                if (data 
                    && data[AppUrl.baggages]) {
                    this.deleteBaggage(data[AppUrl.baggages], flight.devices);
                }
                
                // Get baggageIds to be used when adding
                // Also create baggage/user object 
                const newBaggages = [];
                const userNewBaggages: any = {};
                Object.keys(newPassenger.baggages)
                    .forEach((baggage) => {
                        newBaggages.push({
                            id: baggage,
                            description: newPassenger.baggages[baggage].description
                        });
                        userNewBaggages[baggage] = true;
                    });

                //replace baggage list with object
                newPassenger.baggages = userNewBaggages;
                this.firebase.object(AppUrl.passengers + '/' + userId)
                    .update(newPassenger);
                newBaggages
                    .forEach(baggage => {
                        this.addBaggage(baggage, userId);
                    })
                //Bind baggage and device
                this.bindBaggageAndDevice(flight, newBaggages);

                passengerObservable.unsubscribe();
            });
    }

    deletePassenger(flight: Flight, userId: string) {
        console.log(userId);
        this.fetchPassenger(userId)
            .subscribe((data) => {
                if (data) {
                    this.firebase.list(AppUrl.passengers)
                        .set(userId, null);
                        
                    if(data[AppUrl.baggages]) {
                        this.deleteBaggage(data[AppUrl.baggages], flight.devices);
                        this.flightsService.deleteUser(flight.number, userId);
                    }   
                }
            })
    }

    fetchPassenger(id: string) {
        return this.firebase.object(AppUrl.passengers + '/' + id)
            .snapshotChanges().map(res => {
                return res.payload.val();
            });
    }

    addBaggage(baggageData: any, userId: string) {
        //set default status value. 1 is start
        const baggageId = baggageData.id;
        const baggage: any = {
            status: "1",
            description:  baggageData.description,
            user: {
                [userId]: true
            }
        }

        this.firebase.list(AppUrl.baggages).set(baggageId, baggage);
    }

    fetchBaggage(id: string) {
        return this.firebase.object(AppUrl.baggages + '/' + id)
            .snapshotChanges().map(res => {
                return res.payload.val();
            });
    }

    deleteBaggage(baggages: any[], deviceId: string) {
        Object.keys(baggages)
            .forEach(baggageId => {
                this.firebase.list(AppUrl.baggages)
                    .set(baggageId, null);
                
                this.firebase.list(AppUrl.devices + '/' + deviceId + '/' + AppUrl.baggages)
                    .set(baggageId, null);
            });
    }

    bindBaggageAndDevice(flight: Flight, baggages: any[]) {
        //fetch deviceId on flight
        const deviceId = flight[AppUrl.devices];
        baggages.forEach(baggage => {
            console.log(flight, baggage);
            this.deviceService.bindBaggageAndDevice(deviceId, baggage.id);
        })
    }
}