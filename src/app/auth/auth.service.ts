import { Router } from '@angular/router';
import { Injectable, OnInit } from '@angular/core';

import { Subject } from 'rxjs/Subject';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import * as firebase from 'firebase';
import { AngularFireDatabase } from 'angularfire2/database';

@Injectable()
export class AuthService {
    user: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    userUrl: string = "users";

    constructor(private fireAuth: AngularFireAuth,
        private firebase: AngularFireDatabase, 
        private router: Router) {
        this.fireAuth.authState
            .subscribe(user => {
              if (user) {
                this.user.next({});
                if(this.router.url === '/login') {
                    this.router.navigate(['/flights']);
                } else {
                    this.router.navigate[this.router.url];
                }
              } else {
                this.user.next(null);
              }
            })
    }

    login(email: string, password: string) {
        const userList = this.firebase.list(this.userUrl)
            .snapshotChanges().map(res => {
                let users = [];
                res.map(function(item) {
                    users.push(item.payload.val());
                });

                users = users.map(user => {
                    return user['email'];
                })

                return users;
            }).subscribe(res => {
                if(res.indexOf(email) > -1) {
                    this.fireAuth.auth.signInWithEmailAndPassword(email, password)
                        .then(response => {
                            console.log(response);            
                        }).catch(error => {
                            alert(error.message);
                        })
                } else {
                    alert('No user with that email found!');
                }
                userList.unsubscribe();
            })
    }

    logout() {
        this.fireAuth.auth.signOut()
            .then(response => {
                this.router.navigate(['/login']);
            })
    }

    signUp(email: string, password: string) {
        return this.fireAuth.auth
        .createUserWithEmailAndPassword(email, password);
    }

    changePassword() {
        // return this.fireAuth
        //     .auth.sendPasswordResetEmail();
    }

    isUserSignedIn() {
        return !!this.user.getValue();
    }

}