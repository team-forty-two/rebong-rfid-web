import { Subject } from 'rxjs/Subject';
import { Router } from '@angular/router';
import { AuthService } from './auth/auth.service';
import { Component, OnInit } from '@angular/core';

import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'RFID Baggage Tracker';
  
  constructor(private authService: AuthService) {}

  ngOnInit() {
  }

  logout() {
    this.authService.logout();
  }

  isUserSignedIn() {
    return this.authService.isUserSignedIn();
  }

}
