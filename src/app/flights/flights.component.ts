import { AddEditFlightComponent } from './add-edit-flight/add-edit-flight.component';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

import { FlightsService } from './flights.service';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Flight } from './flight/flight.model';

@Component({
  selector: 'app-flights',
  templateUrl: './flights.component.html',
  styleUrls: ['./flights.component.css']
})
export class FlightsComponent implements OnInit {
  flights: Flight[];
  

  constructor(private modalService: NgbModal,
    private flightsService: FlightsService) { }

  ngOnInit() {
    this.flightsService.fetchFlights()
      .subscribe((data: Flight[]) => {
        data.map(flight => {
          if(flight.devices) {
            flight.devices = Object.keys(flight.devices)[0];
          }
          return flight;
        });
        this.flights = data;
      });
  }

  openAddModal() {
    const modal = this.modalService.open(AddEditFlightComponent);
  }
}
