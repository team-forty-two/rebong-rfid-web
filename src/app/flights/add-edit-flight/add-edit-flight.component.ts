import { DeviceService } from './../../devices/device.service';
import { Flight } from './../flight/flight.model';
import { FlightsService } from './../flights.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl, FormControl } from '@angular/forms';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { Device } from '../../devices/device.model';

@Component({
  selector: 'app-add-edit-flight',
  templateUrl: './add-edit-flight.component.html',
  styleUrls: ['./add-edit-flight.component.css']
})
export class AddEditFlightComponent implements OnInit {
  modalTitle: string = 'Add Flight';
  flightDetails: Flight;
  isAddRequest: boolean = true;
  devices: Observable<Device[]>;
  newFlightForm: FormGroup = this.fb.group({
    number: ["", Validators.required],
    destination: ["", Validators.required],
    departure: ["", Validators.required],
    devices: ["", Validators.required]
  });

  constructor(private fb: FormBuilder, 
    public activeModal: NgbActiveModal,
    private flightsService: FlightsService,
    private deviceService: DeviceService) { }

  ngOnInit() {

    this.devices = this.deviceService.fetchDevices();

    if(this.flightDetails) {
      this.modalTitle = 'Update Flight';
      this.isAddRequest = false;
      this.newFlightForm.controls['number'].setValue(this.flightDetails.number);
      this.newFlightForm.controls['destination'].setValue(this.flightDetails.destination);
      this.newFlightForm.controls['departure'].setValue(this.flightDetails.departure);
      if(this.flightDetails.devices) {
        this.newFlightForm.controls['devices'].setValue(this.flightDetails.devices);
      }
      this.newFlightForm.controls['number'].disable();
    }
  }

  onSubmit() {
    if (this.newFlightForm.valid) {
      if(this.isAddRequest) {
        this.flightsService.addFlight(this.newFlightForm.value);
      } else {
        this.flightsService.updateFlight(this.flightDetails.number, this.newFlightForm.value);
      }
      
      this.newFlightForm.reset();
      this.activeModal.close();
    } else {
      this.validateAllFormFields(this.newFlightForm);      
    }
  }

  isFieldValid(field: string) {
    return !this.newFlightForm.get(field).valid && this.newFlightForm.get(field).touched;
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  getErrorMessage(field: string) {
    if(this.isFieldValid(field)) {
      const errors = this.newFlightForm.get(field).errors;
      let errorMsgs = [];

      Object.keys(errors).forEach(error => {
        if(errors[error]) {
          if(error == 'required') {
            errorMsgs.push('Please enter your ' + field);
          } else {
            if (error == 'email') {
              errorMsgs.push('Please enter correct email address');
            } else if (error == 'pattern') {
              errorMsgs.push('Please enter valid ' + field);
            }
          }
        }
      })

      return errorMsgs;
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

}
