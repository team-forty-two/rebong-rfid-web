import { AppUrl } from './../shared/firebase-uri.model';
import { AngularFireDatabase  } from 'angularfire2/database';
import { Injectable } from '@angular/core';
import { Flight } from './flight/flight.model';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/mergeMap';

@Injectable()
export class FlightsService {
    flightUrl: string = "flights";
    passengerUrl: string = "passengers";
    deviceUrl: string = "devices";
    
    constructor(private firebase: AngularFireDatabase) {}

    fetchFlights() {
        return this.firebase.list(this.flightUrl).valueChanges();
    }

    addFlight(flight: Flight) {
        const origFlight = Object.assign({}, flight);
        flight = this.setDevice(flight);
        this.addDevice(origFlight);
        this.firebase.list(this.flightUrl).set(flight.number, flight);
    }

    updateFlight(flightNumber: string, flight: Flight) {
        const origFlight = Object.assign({}, flight);
        flight = this.setDevice(flight);

        this.updateDevice(flightNumber, origFlight)
            .subscribe(() => {
                this.firebase.list(this.flightUrl)
                    .update(flightNumber, flight);
            })
    }

    deleteFlight(id: string) {
        const flightObserver = this.fetchFlight(id)
            .subscribe((data) => {
                if(data){
                    this.firebase.list(this.flightUrl)
                        .set(id, null);

                    if(data[this.passengerUrl]) {
                        Object.keys(data[this.passengerUrl])
                            .forEach(userId => {
                                this.firebase.list(this.passengerUrl)
                                    .set(userId, null);
                            });
                        
                        this.firebase.list(this.flightUrl)
                            .set(id, null);
                    }

                    if(data[AppUrl.devices]) {
                        this.deleteDevice(data[AppUrl.devices]);
                    }

                    flightObserver.unsubscribe();
                }
            })
    }

    fetchFlight(id: string) {
        return this.firebase.object(this.flightUrl + '/' + id)
            .valueChanges().map((flight: Flight) => {
                if(flight[AppUrl.devices]) {
                    flight[AppUrl.devices] = Object.keys(flight[AppUrl.devices])[0];
                }

                return flight;
            });
    }

    addUser(flightNumber: string, id: string) {
        this.firebase.object(this.flightUrl + '/' + flightNumber + '/' + this.passengerUrl)
            .update({
                [id] : true
            });
    }

    deleteUser(flightNumber: string, id: string) {
        const url = this.flightUrl + '/' + flightNumber + '/' + this.passengerUrl + '/' + id;
        this.firebase.object(url)
            .set(null);
    }

    setDevice(flight: Flight) {
        if(flight.devices) {
            flight.devices = {
                [flight.devices]: true
            };
        }

        return flight;
    }

    addDevice(flight: Flight) {
        this.firebase.object(this.deviceUrl + '/' + flight.devices + '/' + AppUrl.flights)
            .update({
                [flight.number]: true
            });
    }

    updateDevice(flightNumber: string, newFlight: Flight) {
        return this.firebase.object(AppUrl.flights + '/' + flightNumber)
            .valueChanges()
            .map((flight:Flight) => {
                //delete old record
                if(flight.devices) {
                    const oldDeviceId = Object.keys(flight.devices)[0];
                    this.firebase.object(this.deviceUrl + '/' + oldDeviceId 
                        + '/' + AppUrl.flights + '/' + flightNumber)
                        .set(null);
                }
                
                //Add new
                this.firebase.object(this.deviceUrl + '/' + newFlight.devices + '/' + AppUrl.flights)
                    .update({
                        [flightNumber]: true
                    });
            })
    }

    deleteDevice(deviceId: string) {
        this.firebase.list(this.deviceUrl)
            .set(deviceId, null);
    }
}
