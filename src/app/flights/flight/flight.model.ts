import { OnInit } from '@angular/core';
export class Flight {
    
    number: string;
    destination: string;
    departure: string;
    devices: any;
    passengers: any;

    constructor(number, destination, departure) {
        this.number = number;
        this.destination = destination;
        this.departure = departure;
    }
}