import { FlightsService } from './../flights.service';
import { Component, OnInit, Input } from '@angular/core';
import { Flight } from './flight.model';

@Component({
  selector: 'app-flight',
  templateUrl: './flight.component.html',
  styleUrls: ['./flight.component.css']
})
export class FlightComponent implements OnInit {
  @Input() flight: Flight;
  passengerCount: number;

  constructor(private flightsService: FlightsService) { }

  deleteFlight(id: string) {
    this.flightsService.deleteFlight(id);
  }

  ngOnInit() {
      this.passengerCount = 0;
  
      if(this.flight.passengers) {
          this.passengerCount = Object.keys(this.flight.passengers).length;
      }
  }

}
